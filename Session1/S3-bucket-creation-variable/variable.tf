variable "bucket_name" {
description = "Name of bucket"
}

variable "acl" {
  description = "Access control list"
  default = "private"
}

variable "bucket_tag" {
  description = "Tag name of bucket"
}

variable "environment" {
  description = "Environment where terraform script is running"
  default = "Dev"
}

variable "versioning" {
  description = "Versioning of bucket"
  default = "true"
}
