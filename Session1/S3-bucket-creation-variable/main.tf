resource "aws_s3_bucket" "bmo-mdm-bucket" {
  bucket = var.bucket_name
  acl    = var.acl

  tags = {
    Name        = var.bucket_tag
    Environment = var.environment
  }

  versioning {
    enabled = var.versioning
  }
}