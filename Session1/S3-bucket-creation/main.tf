resource "aws_s3_bucket" "bmo-mdm-bucket" {
  bucket = "bmo-mdm-bucket-20200304-dev"
  acl    = "private"

  tags = {
    Name       = "bmo-mdm-bucket-20200304-dev"
    Environment = "Dev"
  }

  versioning {
    enabled = true
  }
}